# Thema11-Dataprocessing-eindopdracht
This repository contains a pipeline that can be used for searching pathogens in a given organism, all this program needs is a human index file, a pathogen index file and some RNAseq data.

## Getting started
### Instalation
Clone the repository into the target directory and change the variables in the config.yaml to your liking
```shell
git clone https://Rinze-Pieter@bitbucket.org/Rinze-Pieter/thema11-dataprocessing-eindopdracht.git
```


### Prerequisities
* Bowtie2
	+ This is a package that can be used for processing sequencing data and for alligning genes
	+ instalation can be done by using the following shell code: ```shell pip install Bowtie2```
	+ website can be found at [http://bowtie-bio.sourceforge.net/bowtie2/index.shtml](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* Samtools
	+ the Samtools package is a package that can be used to process .sam files very efficent
	+ instalation can be done by using the following shell code : ```shell pip install Samtools```
	+ the website for this package can be found at [http://samtools.sourceforge.net/](http://samtools.sourceforge.net/)

### Usage
the code can just run without any parameters, if you want to changes this you will need to change the parameters in the config.yaml file that can be found in the map __Eindproject/__.
```shell
snakemake --snakefile PipelinePathogen
```

## Files included
* __Eindproject/Scripts/getAccesssionName.py__ - this gets the Accessionname from the pathogen fasta file
* __Eindproject/Scripts/getAccessionName__ - this is the snakemake file that runs the script above
* __Eindproject/Scripts/RemoveFailed__ - this snakemake file removes all the allignments that have failed to allign
* __Eindproject/Scripts/SamToBam__ - this snakemake file converts Samfiles into Bamfiles by using samtools
* __Eindproject/Scripts/BowtieIndex__ - this snakemake file creates the index maps for humans and the pathogen
* __Eindproject/Scripts/Bowtie2Ref__ - this snakemake file runs the given samples to the human index and the pathogen index
* __Eindproject/PipelinePathogen__ - the snakemake file that combines all other rules
* __Eindproject/inputs/__ - some test files
* __Eindproject/human_sub.fna__ - a test file for the human index
* __Eindproject/pathogen_sub.fna__ - a test file for the pathogen index
* __Eindproject/config.yaml__ - This is the config file that can be used to change some of the parameters
* __venv/__ - the virtual enviroment
* __dag1.png__ - This is the diagram of the code 

## Authors
* __[Rinze-Pieter Jonker](https://bitbucket.org/%7Bc74c81b3-fb4f-42e0-b9a5-e531521e9f77%7D/)__ (ri.jonker@st.hanze.nl)