#!/usr/bin/env python3
"""
moet nog config file worden toegevoegd
"""

# IMPORTS
import glob
import os
import sys
import yaml

# METADATA


# CODE
def getGenomeDict(pathogen_fasta):
    """
    this function gets all the genomes from a given fasta file and returns a genome dictionary
    :return:
    """
    genome_dict = dict()

    with open(pathogen_fasta) as allgenomes:
        for line in allgenomes:
            if line.startswith(">gi"):
                genome = line.split(">")[1].split(",")[0]
                refname = genome.split("| ")[0]
                organism = genome.split("| ")[1]
                genome_dict[refname] = organism

            elif line.startswith(">JPKZ") or line.startswith(">MIEF") or line.startswith(">LL") or line.startswith(">AWXF") or line.startswith("EQ") or line.startswith(">NW_") or line.startswith(">LWMK") or line.startswith(">NZ_") or line.startswith(">NC_") or line.startswith(">KT"):
                genome = line.split(">")[1].split(",")[0]
                refname = genome.split(" ")[0]
                organismName = genome.split(" ")[0]
                organism = "".join(organismName)
                genome_dict[refname] = organism

    return genome_dict


def AccessionToName(genome_dict, input_files):
    scientific_names = "Scientific_names/"
    single_names = "Single_names/"
    if not os.path.isdir(scientific_names):
        os.mkdir(scientific_names)
    if not os.path.isdir(single_names):
        os.mkdir(single_names)

    for file in input_files:
        accession_nrs = list()
        # file_path = "accession/" + file + ".txt"
        file_path = file

        for line in open(file_path):
            accession_nrs.append(line.split("\t")[0])

        pathogen_names = list()
        output_file = scientific_names + file[10:]
        with open(output_file, "w") as openout:
            for number in accession_nrs:
                if number in genome_dict:
                    pathogen_names.append(genome_dict[number])

            for i in pathogen_names:
                openout.write(i + "\n")

        with open(output_file, "r") as outfile:
            final_pathogen_file = single_names + file[10:]
            open_final_file = open(final_pathogen_file, "w")

            pathogens = list()

            for line in outfile:
                newline = line.split(" ")[:2]
                if not newline in pathogens:
                    pathogens.append(newline)

            for pathogen in pathogens:
                open_final_file.write("".join(pathogen) + "\n")



def main():
    with open("config.yaml") as configfile:
        cfg = yaml.load(configfile)
        pathogen_fasta = cfg["pathogen"]
        genome_dict = getGenomeDict(pathogen_fasta=pathogen_fasta)
        # genome_dict = getGenomeDict(pathogen_fasta=sys.argv[1])

        input_files = [x for x in glob.glob("accession/" + "*.txt")]
        #
        AccessionToName(genome_dict=genome_dict, input_files=input_files)
        # AccessionToName(genome_dict=genome_dict, input_files=sys.argv[2:])
    return 0


if __name__ == "__main__":
    sys.exit(main())
